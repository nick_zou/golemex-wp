const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

var babelLoader = {test: /\.js$/, loader: 'babel-loader', exclude:/node_modules/, query: {presets: ['es2015']}};
var rawLoader = {test: /\.html$/, loader: 'raw-loader', exclude: /node_modules/};
var styleLoader = {test: /\.css$/, loader: "style-loader!css-loader", exclude: /node_modules/};
var scssLoader = {test: /\.scss$/, loader: "style-loader!css-loader?sourceMap!postcss-loader?sourceMap!sass-loader?sourceMap", exclude: /node_modules/};
var scssLoaderExtracted = {test: /\.scss$/, use: ExtractTextPlugin.extract({
  fallback: "style-loader",
  use: "css-loader?sourceMap!postcss-loader?sourceMap!sass-loader?sourceMap"
}), exclude: /node_modules/};
var urlLoader = {test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/, loader: 'url-loader'};

var defaultModuleRules = [
  babelLoader,
  rawLoader,
  urlLoader
];

var CSSModuleRules = defaultModuleRules.slice();
CSSModuleRules.push(styleLoader, scssLoader);

var extractedCSSModuleRules = defaultModuleRules.slice();
extractedCSSModuleRules.push(styleLoader, scssLoaderExtracted);

var config = {
  watch: true,
  devtool: 'source-map',
  plugins: [
    new CopyWebpackPlugin([{ from: './src/theme', to: '../../www.golemex.dev.cc/wp-content/themes/golemex' }])
  ]
};

var criticalConfig = Object.assign({}, config, {
  name: "critical",
  entry: __dirname + "/src/entry/critical.js",
  output: {
    path: __dirname + "/src/theme",
    filename: "js/critical.js"
  },
  module: {
      rules: CSSModuleRules
    },
});

var generalConfig = Object.assign({}, config, {
  name: "general",
  entry: __dirname + "/src/entry/app.js",
  output: {
    path: __dirname + "/src/theme",
    filename: "js/app.js"
  },
  module: {
      rules: extractedCSSModuleRules
  },
  plugins: [
    new ExtractTextPlugin('css/styles.css')
  ],
});

module.exports = [
  criticalConfig, generalConfig
];
