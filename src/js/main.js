let body = document.getElementsByTagName("body")[0];
let loader = document.getElementById('page-loader');
let header = document.getElementById('site-header');

let getBodyMarginTop = function () {
    body.style.marginTop = header.clientHeight + "px";
};

let scrolledMenu = function() {
    if (window.pageYOffset >= header.clientHeight) {
        if (!header.classList.contains("scrolled")) {
            header.classList.add("scrolled");
        }
    } else {
        if (header.classList.contains("scrolled")) {
            header.classList.remove("scrolled");
        }
    }
}

// let loaderRemove = function () {
//     loader.classList.add("hidden");
//     body.style.overflowY = "initial";
// }

window.addEventListener('load', function() {
    window.addEventListener('resize', getBodyMarginTop);
    window.addEventListener('scroll', scrolledMenu);
    getBodyMarginTop();
}, false);