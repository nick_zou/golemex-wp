<?php

function golemex_enqueue_global_resources() {
  wp_enqueue_style('golemex-style', get_stylesheet_uri());
  wp_enqueue_script('critical-styles');
  wp_enqueue_style('fontawesome');
  wp_enqueue_style('styles');
  wp_enqueue_script('app');
}

add_action('wp_enqueue_scripts', 'golemex_enqueue_global_resources');
