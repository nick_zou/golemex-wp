<?php
    function golemex_theme_setup() {
        register_nav_menus(
            array(
                'main' => __('Main Menu'),
                'user' => __('User Menu'),
                'footer-1' => __('Footer Menu 1'),
                'footer-2' => __('Footer Menu 2'),
                'footer-3' => __('Footer Menu 3')
            )
        );

        add_theme_support('post-thumbnails');
    }

    add_action('after_setup_theme', 'golemex_theme_setup');