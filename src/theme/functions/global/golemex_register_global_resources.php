<?php

function golemex_register_global_resources() {
  wp_register_script('critical-styles', get_template_directory_uri() . '/js/critical.js', array() , 1, false);
  wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js' , array(), 1, true);
  wp_register_script('app', get_template_directory_uri() . '/js/app.js', array() , 1, true);
  wp_register_style('styles', get_template_directory_uri() . '/css/styles.css', array(), '1.0.4', 'all');
  wp_register_style('fontawesome', 'https://pro.fontawesome.com/releases/v5.0.13/css/all.css', array(), '5.0.13', all);
}

add_action('init', 'golemex_register_global_resources');
