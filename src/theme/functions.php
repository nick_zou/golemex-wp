<?php

require get_template_directory() . '/functions/global/golemex_register_global_resources.php';
require get_template_directory() . '/functions/global/golemex_enqueue_global_resources.php';
require get_template_directory() . '/functions/global/golemex_theme_setup.php';
require get_template_directory() . '/functions/global/golemex_widgets.php';

?>